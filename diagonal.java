import java.util.Arrays;

public class diagonal {
   
    
        public static void main(String[] args) {
            int [] x = {1,2,3,4,5,6,7,8,9,10,11,12} ;  
            System.out.println(Arrays.binarySearch(x, 12));

            int [] y = {1,2,3,4,5,6,7,8,9,10,11,12} ;  
            int [] z = {1,2,3,4,5,6,7,8,9,10,11,10002} ;  
            System.out.println(Arrays.equals(y, z));

            int [] primos = {1,2,3,5};
            int [] copiaClonado;
            int [] copiaReferencia;

            copiaClonado = primos.clone();
            copiaClonado[0] = 0;

            copiaReferencia = primos;

            copiaReferencia[1]= 0;

            System.out.println(Arrays.toString(primos));            
            System.out.println(Arrays.toString(copiaReferencia));
            System.out.println(Arrays.toString(copiaClonado));

        }    
        
    
}
